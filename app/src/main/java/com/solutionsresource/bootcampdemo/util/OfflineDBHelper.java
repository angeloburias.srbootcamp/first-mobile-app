package com.solutionsresource.bootcampdemo.util;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.solutionsresource.bootcampdemo.model.Login;
import com.solutionsresource.bootcampdemo.model.User;

import java.util.ArrayList;

public class OfflineDBHelper extends SQLiteOpenHelper {

    public OfflineDBHelper(Context context) {
        super(context, Constants.OFFLINE_DATABASE_NAME, null, Constants.OFFLINE_DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase sqLiteDatabase) {
        // Workaround on android pie having Write-Ahead Logging enabled by default;
        sqLiteDatabase.disableWriteAheadLogging();
        super.onOpen(sqLiteDatabase);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        // Create User Table
        sqLiteDatabase.execSQL("CREATE TABLE "
                + Constants.USER_TABLE
                + "("
                + Constants.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + Constants.USERNAME + " TEXT NOT NULL, "
                + Constants.PASSWORD + " TEXT NOT NULL, "
                + Constants.NAME + " TEXT NOT NULL, "
                + Constants.EMAIL_ADDRESS + " TEXT, "
                + Constants.CONTACT_NUMBER + " TEXT, "
                + Constants.GENDER + " TEXT"
                + ");"
        );
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        dropTables(sqLiteDatabase);
    }

    public void dropTables(SQLiteDatabase sqLiteDatabase) {
        // Handle table alteration here in case you dont want the existing data to be deleted
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + Constants.USER_TABLE);
        onCreate(sqLiteDatabase);
    }

    public Object create(String table, Object object) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        ContentValues currentValues = getContentValues(table, object);
        long result = sqLiteDatabase.insert(table, null, currentValues);
        return get(table, Constants.ID, String.valueOf(result));
    }

    public Object alter(String table, Object object, String id) {
        this.getWritableDatabase().update(table, getContentValues(table, object), Constants.ID + " = ?", new String[]{id});
        return get(table, Constants.ID, id);
    }

    public int delete(String table, String id) {
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        int result = sqLiteDatabase.delete(table, Constants.ID + " = ?", new String[]{id});
        return result;
    }

    public Object get(String table, String field, String id) {
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM " + table
                + " WHERE " + field + " = ?", new String[]{id});
        Object object = null;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            object = getObjectValues(table, cursor);
        }
        cursor.close();
        return object;
    }

    public Object login(Login login) {
        String table = Constants.USER_TABLE;
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM " + table
                + " WHERE " + Constants.USERNAME + " = ? AND " + Constants.PASSWORD + " = ?", new String[]{login.getUsername(), login.getPassword()});
        Object object = null;
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            object = getObjectValues(table, cursor);
        }
        cursor.close();
        return object;
    }

    public ArrayList<Object> getAll(String table, int limit, int page) {
        Cursor cursor = this.getReadableDatabase().rawQuery("SELECT * FROM " + table
                + " ORDER BY " + Constants.ID + " DESC LIMIT " + limit + " OFFSET " + limit * --page, null);
        cursor.moveToFirst();
        ArrayList<Object> list = new ArrayList<>();
        while (!cursor.isAfterLast()) {
            list.add(getObjectValues(table, cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    private ContentValues getContentValues(String table, Object object) {
        ContentValues contentValues = new ContentValues();
        // Use switch case for reusability, define new tables here
        switch (table) {
            case Constants.USER_TABLE:
                if (!(object instanceof User))
                    break;
                User user = (User) object;
                contentValues.put(Constants.USERNAME, user.getUsername());
                contentValues.put(Constants.PASSWORD, user.getPassword());
                contentValues.put(Constants.NAME, user.getName());
                contentValues.put(Constants.EMAIL_ADDRESS, user.getEmailAddress());
                contentValues.put(Constants.CONTACT_NUMBER, user.getContactNumber());
                break;
        }
        return contentValues;
    }

    @SuppressLint("Range")
    private Object getObjectValues(String table, Cursor cursor) {
        // Use switch case for reusability, define new tables here
        switch (table) {
            case Constants.USER_TABLE:
                return new User(cursor.getInt(cursor.getColumnIndex(Constants.ID)),
                        cursor.getString(cursor.getColumnIndex(Constants.USERNAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.PASSWORD)),
                        cursor.getString(cursor.getColumnIndex(Constants.NAME)),
                        cursor.getString(cursor.getColumnIndex(Constants.EMAIL_ADDRESS)),
                        cursor.getString(cursor.getColumnIndex(Constants.CONTACT_NUMBER)));
            default:
                return null;
        }
    }
}
