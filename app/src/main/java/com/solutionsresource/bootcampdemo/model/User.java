package com.solutionsresource.bootcampdemo.model;

import android.os.Parcel;
import android.os.Parcelable;

import lombok.Data;

@Data
public class User implements Parcelable {

    public static final String TAG = User.class.getSimpleName();
    private int id;
    private String username, password, name, emailAddress, contactNumber;

    public User(int id, String username, String password, String name, String emailAddress, String contactNumber) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.name = name;
        this.emailAddress = emailAddress;
        this.contactNumber = contactNumber;
    }

    public User(String username, String password, String name, String emailAddress, String contactNumber) {
        this.username = username;
        this.password = password;
        this.name = name;
        this.emailAddress = emailAddress;
        this.contactNumber = contactNumber;
    }

    // setup parcelable
    public static final Parcelable.Creator CREATOR = new Parcelable.Creator() {
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        public User[] newArray(int size) {
            return new User[size];
        }
    };

    private User(Parcel in) {
        readFromParcel(in);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(username);
        parcel.writeString(password);
        parcel.writeString(name);
        parcel.writeString(emailAddress);
        parcel.writeString(contactNumber);
    }

    private void readFromParcel(Parcel parcel) {
        // the order needs to be the same as in writeToParcel() method
        this.id = parcel.readInt();
        this.username = parcel.readString();
        this.password = parcel.readString();
        this.name = parcel.readString();
        this.emailAddress = parcel.readString();
        this.contactNumber = parcel.readString();
    }
}
