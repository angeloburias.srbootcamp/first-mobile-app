package com.solutionsresource.bootcampdemo.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.solutionsresource.bootcampdemo.R;
import com.solutionsresource.bootcampdemo.model.User;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener, DialogInterface.OnClickListener {

    private EditText editTextUsername;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private EditText editTextName;
    private EditText editTextEmailAddress;
    private EditText editTextContactNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);
        editTextConfirmPassword = findViewById(R.id.editTextConfirmPassword);
        editTextName = findViewById(R.id.editTextName);
        editTextEmailAddress = findViewById(R.id.editTextEmailAddress);
        editTextContactNumber = findViewById(R.id.editTextContactNumber);

        Button btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        Button btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnSubmit) {
            // basic client side validation
          //  if (validate()) {
                User user = new User(editTextUsername.getText().toString().trim(),
                        editTextPassword.getText().toString().trim(),
                        editTextName.getText().toString().trim(),
                        editTextEmailAddress.getText().toString().trim(),
                        editTextContactNumber.getText().toString().trim());
                // return result on main activity
                Intent intent = new Intent();
                intent.putExtra(User.TAG, user);
                intent.putExtra("name", editTextName.getText().toString().trim());
                setResult(Activity.RESULT_OK, intent);
                finish();
         //   }
        } else {
            onBackPressed();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to go back?")
                .setNegativeButton(getString(R.string.btn_cancel), null)
                .show();
    }


    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        finish();
    }
    private boolean validate() {
        if (editTextUsername.getText().toString().isEmpty()) {
            editTextUsername.setError("Please fill out username");
            editTextUsername.requestFocus();
            return false;
        } else if (editTextUsername.getText().toString().trim().length() < 6) {
            editTextUsername.setError("Username must be at least 6 characters");
            editTextUsername.requestFocus();
            return false;
        } else if (editTextPassword.getText().toString().isEmpty()) {
            editTextPassword.setError("Please fill out password");
            editTextPassword.requestFocus();
            return false;
        } else if (editTextPassword.getText().toString().trim().length() < 6) {
            editTextPassword.setError("Password must be at least 6 characters");
            editTextPassword.requestFocus();
            return false;
        } else if (editTextConfirmPassword.getText().toString().isEmpty()) {
            editTextConfirmPassword.setError("Please confirm your password");
            editTextConfirmPassword.requestFocus();
            return false;
        } else if (!editTextPassword.getText().toString().trim().equals(editTextConfirmPassword.getText().toString().trim())) {
            editTextConfirmPassword.setError("Your password must match and and must be be at least 6 characters");
            editTextConfirmPassword.requestFocus();
            return false;
        } else if (editTextName.getText().toString().isEmpty()) {
            editTextName.setError("Please fill out name");
            editTextName.requestFocus();
            return false;
        } else if (editTextContactNumber.getText().toString().length() > 0 && !editTextContactNumber.getText().toString().matches("\\d{6,10}")) {
            editTextContactNumber.setError("Invalid contact number");
            editTextContactNumber.requestFocus();
            return false;
        }
        return true;
    }
}
