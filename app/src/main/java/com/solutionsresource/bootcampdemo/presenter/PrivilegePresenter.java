package com.solutionsresource.bootcampdemo.presenter;

import com.solutionsresource.bootcampdemo.model.Privilege;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PrivilegePresenter {

    private final PrivilegeService service;
    private Callback callback;

    public PrivilegePresenter(Callback callback) {
        this.callback = callback;

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://54.169.21.244:8190/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(PrivilegeService.class);
    }

    public void getAllPrivilege() {
        service.getAllPrivilege().enqueue(new retrofit2.Callback<Privilege>() {
            @Override
            public void onResponse(Call<Privilege> call, Response<Privilege> response) {
                if (response.isSuccessful()) {
                    callback.getAllPrivilegeSuccess(response.body());
                } else {
                    callback.getAllPrivilegeFailed();
                }
            }

            @Override
            public void onFailure(Call<Privilege> call, Throwable t) {
                callback.getAllPrivilegeFailed();
            }
        });
    }

    public void postPrivilege(String name) {
        service.postPrivilege(new Privilege.Data(name)).enqueue(new retrofit2.Callback<Privilege>() {
            @Override
            public void onResponse(Call<Privilege> call, Response<Privilege> response) {
                if (response.isSuccessful()) {
                    Privilege privilege = response.body();
                    callback.postPrivilegeSuccess(privilege.getMessage());
                } else {
                    callback.postPrivilegeFailed();
                }
            }

            @Override
            public void onFailure(Call<Privilege> call, Throwable t) {
                callback.postPrivilegeFailed();
            }
        });
    }

    public void putPrivilege(int id, String name) {
        service.putPrivilege(id, new Privilege.Data(name)).enqueue(new retrofit2.Callback<Privilege>() {
            @Override
            public void onResponse(Call<Privilege> call, Response<Privilege> response) {
                if (response.isSuccessful()) {
                    Privilege privilege = response.body();
                    callback.putPrivilegeSuccess(privilege.getMessage());
                } else {
                    callback.putPrivilegeFailed();
                }
            }

            @Override
            public void onFailure(Call<Privilege> call, Throwable t) {
                callback.putPrivilegeFailed();
            }
        });
    }

    public void deletePrivilege(int id) {
        service.deletePrivilege(id).enqueue(new retrofit2.Callback<Privilege>() {
            @Override
            public void onResponse(Call<Privilege> call, Response<Privilege> response) {
                if (response.isSuccessful()) {
                    Privilege privilege = response.body();
                    callback.deletePrivilegeSuccess(privilege.getMessage());
                } else {
                    callback.deletePrivilegeFailed();
                }
            }

            @Override
            public void onFailure(Call<Privilege> call, Throwable t) {
                callback.deletePrivilegeFailed();
            }
        });
    }

    public interface Callback {
        void getAllPrivilegeSuccess(Privilege privilege);

        void getAllPrivilegeFailed();

        void postPrivilegeSuccess(String message);

        void postPrivilegeFailed();

        void putPrivilegeSuccess(String message);

        void putPrivilegeFailed();

        void deletePrivilegeSuccess(String message);

        void deletePrivilegeFailed();
    }
}
