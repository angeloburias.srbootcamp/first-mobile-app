package com.solutionsresource.bootcampdemo.util;

import com.solutionsresource.bootcampdemo.model.Privilege;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PrivilegeService {
    @GET("privilege/return/all")
    Call<Privilege> returnAllPrivilege();
}