package com.solutionsresource.bootcampdemo.presenter;

import com.solutionsresource.bootcampdemo.model.Privilege;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface PrivilegeService {
    @GET("privilege/return/all")
    Call<Privilege> getAllPrivilege();

    @POST("add/privilege")
    Call<Privilege> postPrivilege(@Body Privilege.Data data);

    @PUT("update/privilege/{id}")
    Call<Privilege> putPrivilege(@Path("id") int id, @Body Privilege.Data data);

    @DELETE("privilege/{id}")
    Call<Privilege> deletePrivilege(@Path("id") int id);
}
