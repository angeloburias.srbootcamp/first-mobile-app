package com.solutionsresource.bootcampdemo.util;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreference {

    public static SharedPreferences getInstance(Context context) {
        return context.getSharedPreferences(Constants.SHARED_PREFERENCE, Context.MODE_PRIVATE);
    }

    public static void storeStringValue(Context context, String name, String value) {
        SharedPreferences.Editor editor = getInstance(context).edit();
        editor.putString(name, value).apply();
    }

    public static String getStringValue(Context context, String name) {
        SharedPreferences sharedPreferences = getInstance(context);
        return sharedPreferences.getString(name, null);
    }

    public static void storeBooleanValue(Context context, String name, boolean value) {
        SharedPreferences.Editor editor = getInstance(context).edit();
        editor.putBoolean(name, value).apply();
    }

    public static boolean getBooleanValue(Context context, String name) {
        SharedPreferences sharedPreferences = getInstance(context);
        return sharedPreferences.getBoolean(name, false);
    }


    public static void removeValue(Context context, String name) {
        SharedPreferences.Editor editor = getInstance(context).edit();
        editor.remove(name).apply();
    }

    public static void clear(Context context) {
        // clear shared preference
        getInstance(context).edit().clear().apply();
    }
}
