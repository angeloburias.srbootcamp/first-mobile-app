package com.solutionsresource.bootcampdemo.activity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.solutionsresource.bootcampdemo.R;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import androidx.appcompat.app.AppCompatActivity;

public class CalculatorActivity extends AppCompatActivity implements View.OnClickListener, View.OnLongClickListener {

    private String current = "0";
    private String holder = "0";
    private Operator operator = Operator.CLEAR;

    public enum Operator {
        DIVIDE,
        MULTIPLY,
        SUBTRACT,
        ADD,
        CLEAR
    }

    private TextView textViewValue;
    private Button btnZero, btnOne, btnTwo, btnThree, btnFour, btnFive, btnSix, btnSeven, btnEight,
            btnNine, btnDecimal, btnEqual, btnDivide, btnMultiply, btnSubtract, btnAdd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        textViewValue = findViewById(R.id.textViewValue);

        // numbers
        btnZero = findViewById(R.id.btnZero);
        btnZero.setOnClickListener(this);
        btnOne = findViewById(R.id.btnOne);
        btnOne.setOnClickListener(this);
        btnTwo = findViewById(R.id.btnTwo);
        btnTwo.setOnClickListener(this);
        btnThree = findViewById(R.id.btnThree);
        btnThree.setOnClickListener(this);
        btnFour = findViewById(R.id.btnFour);
        btnFour.setOnClickListener(this);
        btnFive = findViewById(R.id.btnFive);
        btnFive.setOnClickListener(this);
        btnSix = findViewById(R.id.btnSix);
        btnSix.setOnClickListener(this);
        btnSeven = findViewById(R.id.btnSeven);
        btnSeven.setOnClickListener(this);
        btnEight = findViewById(R.id.btnEight);
        btnEight.setOnClickListener(this);
        btnNine = findViewById(R.id.btnNine);
        btnNine.setOnClickListener(this);
        // operators
        btnDecimal = findViewById(R.id.btnDecimal);
        btnDecimal.setOnClickListener(this);
        btnEqual = findViewById(R.id.btnEqual);
        btnEqual.setOnClickListener(this);
        btnEqual.setOnLongClickListener(this);
        btnDivide = findViewById(R.id.btnDivide);
        btnDivide.setOnClickListener(this);
        btnMultiply = findViewById(R.id.btnMultiply);
        btnMultiply.setOnClickListener(this);
        btnSubtract = findViewById(R.id.btnSubtract);
        btnSubtract.setOnClickListener(this);
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnZero:
                setValue("0");
                break;
            case R.id.btnOne:
                setValue("1");
                break;
            case R.id.btnTwo:
                setValue("2");
                break;
            case R.id.btnThree:
                setValue("3");
                break;
            case R.id.btnFour:
                setValue("4");
                break;
            case R.id.btnFive:
                setValue("5");
                break;
            case R.id.btnSix:
                setValue("6");
                break;
            case R.id.btnSeven:
                setValue("7");
                break;
            case R.id.btnEight:
                setValue("8");
                break;
            case R.id.btnNine:
                setValue("9");
                break;
            case R.id.btnDecimal:
                setValue(".");
                break;
            case R.id.btnDivide:
                if (operator == Operator.CLEAR) {
                    holder = current;
                    current = "0";
                }
                operator = Operator.DIVIDE;
                break;
            case R.id.btnMultiply:
                if (operator == Operator.CLEAR) {
                    holder = current;
                    current = "0";
                }
                operator = Operator.MULTIPLY;
                break;
            case R.id.btnSubtract:
                if (operator == Operator.CLEAR) {
                    holder = current;
                    current = "0";
                }
                operator = Operator.SUBTRACT;
                break;
            case R.id.btnAdd:
                if (operator == Operator.CLEAR) {
                    holder = current;
                    current = "0";
                }
                operator = Operator.ADD;
                break;
            case R.id.btnEqual:
                double result = 0;
                switch (operator) {
                    case DIVIDE:
                        result = Double.parseDouble(holder) / Double.parseDouble(current);
                        break;
                    case MULTIPLY:
                        result = Double.parseDouble(holder) * Double.parseDouble(current);
                        break;
                    case SUBTRACT:
                        result = Double.parseDouble(holder) - Double.parseDouble(current);
                        break;
                    case ADD:
                        result = Double.parseDouble(holder) + Double.parseDouble(current);
                        break;
                }
                holder = String.valueOf(result);
                current = "0";
                operator = Operator.CLEAR;
                setValue(roundDoubleFormat(String.valueOf(result)));
                break;
        }
    }

    private void setValue(String value) {
        if (value.equals("0.0"))
            value = "0";
        if (current.contains(".") && value.equals("."))
            return;
        if (!current.equals("0")) {
            current += value;
        } else {
            current = value;
        }
        textViewValue.setText(stringToDoubleFormat(current));
    }

    @Override
    public boolean onLongClick(View view) {
        Toast.makeText(this, "Clear", Toast.LENGTH_SHORT).show();
        btnEqual.performClick();
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {
        Log.e("onPointerCaptureChanged", String.valueOf(hasCapture));
    }

    public static String stringToDoubleFormat(String value) {
        try {
            StringBuilder pattern = new StringBuilder("#,###");
            if (value.contains(".")) {
                String[] split = value.split("\\.");
                if (split.length > 1) {
                    for (int i = 0; i < split[1].length(); i++) {
                        if (i == 0) {
                            pattern.append(".");
                        }
                        pattern.append("0");
                    }
                } else {
                    pattern.append(".");
                }
            }
            double result = Double.parseDouble(value);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern(pattern.toString());
            return formatter.format(result);
        } catch (Exception e) {
            return value;
        }
    }

    public static String roundDoubleFormat(String value) {
        try {
            double result = Double.parseDouble(value);
            DecimalFormat formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
            formatter.applyPattern("#,###.##");
            formatter.setRoundingMode(RoundingMode.CEILING);
            return formatter.format(result);
        } catch (Exception e) {
            return value;
        }
    }
}