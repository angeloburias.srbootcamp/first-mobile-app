package com.solutionsresource.bootcampdemo.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solutionsresource.bootcampdemo.R;
import com.solutionsresource.bootcampdemo.model.Privilege;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class PrivilegeAdapter extends RecyclerView.Adapter<PrivilegeAdapter.ViewHolder> {

    private Callback callback;
    private List<Privilege.Data> dataList;

    public PrivilegeAdapter(Callback callback, List<Privilege.Data> dataList) {
        this.callback = callback;
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).
                inflate(R.layout.item_privilege, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Privilege.Data data = dataList.get(position);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callback.selectedItem(data);
            }
        });
        holder.container.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                callback.deleteItem(data);
                return false;
            }
        });
        holder.textViewName.setText(data.getName());
        holder.textViewId.setText(String.valueOf(data.getId()));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout container;
        public TextView textViewName, textViewId;

        public ViewHolder(View itemView) {
            super(itemView);
            container = itemView.findViewById(R.id.container);
            textViewName = itemView.findViewById(R.id.textViewName);
            textViewId = itemView.findViewById(R.id.textViewId);
        }
    }

    public interface Callback {
        void selectedItem(Privilege.Data data);

        void deleteItem(Privilege.Data data);
    }
}
