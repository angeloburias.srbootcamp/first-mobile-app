package com.solutionsresource.bootcampdemo.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.solutionsresource.bootcampdemo.R;
import com.solutionsresource.bootcampdemo.adapter.PrivilegeAdapter;
import com.solutionsresource.bootcampdemo.model.Privilege;
import com.solutionsresource.bootcampdemo.presenter.PrivilegePresenter;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class PrivilegeActivity extends AppCompatActivity implements PrivilegePresenter.Callback, PrivilegeAdapter.Callback,View.OnClickListener {

    private PrivilegePresenter presenter;
    private RecyclerView recyclerView;
    private EditText editTextName;
    private Button btnSubmit;
    private ProgressBar progressBar;
    private Privilege.Data data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privilege);
        presenter = new PrivilegePresenter(this);
        recyclerView = findViewById(R.id.recyclerView);
        editTextName = findViewById(R.id.editTextName);
        btnSubmit = findViewById(R.id.btnSubmit);
        btnSubmit.setOnClickListener(this);
        progressBar = findViewById(R.id.progressBar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.getAllPrivilege();
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void getAllPrivilegeSuccess(Privilege privilege) {
        PrivilegeAdapter adapter = new PrivilegeAdapter(this, privilege.getDataList());
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void getAllPrivilegeFailed() {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Get All Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void postPrivilegeSuccess(String message) {
        btnSubmit.setEnabled(true);
        editTextName.setText(""); // clear edit text
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        presenter.getAllPrivilege();
    }

    @Override
    public void postPrivilegeFailed() {
        btnSubmit.setEnabled(true);
        editTextName.setText(""); // clear edit text
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Post Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void putPrivilegeSuccess(String message) {
        btnSubmit.setEnabled(true);
        btnSubmit.setText("Submit");
        editTextName.setText(""); // clear edit text
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        presenter.getAllPrivilege();
    }

    @Override
    public void putPrivilegeFailed() {
        btnSubmit.setEnabled(true);
        btnSubmit.setText("Submit");
        editTextName.setText(""); // clear edit text
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Put Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void deletePrivilegeSuccess(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        presenter.getAllPrivilege();
    }

    @Override
    public void deletePrivilegeFailed() {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(this, "Post Failed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void selectedItem(Privilege.Data data) {
        this.data = data;
        if (data != null) {
            btnSubmit.setText("Update");
            editTextName.setText(data.getName());
        }
    }

    @Override
    public void deleteItem(Privilege.Data data) {
        new AlertDialog.Builder(this)
                .setMessage("Are you sure you want to delete " + data.getName() + "?")
                .setPositiveButton(getString(R.string.btn_ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        presenter.deletePrivilege(data.getId());
                        progressBar.setVisibility(View.VISIBLE);
                    }
                })
                .setNegativeButton(getString(R.string.btn_cancel), null)
                .show();
    }

    @Override
    public void onClick(View view) {
        btnSubmit.setEnabled(false);
        if (btnSubmit.getText().equals("Update")) {
            presenter.putPrivilege(data.getId(), editTextName.getText().toString().trim());
        } else {
            presenter.postPrivilege(editTextName.getText().toString().trim());
        }
    }
}
