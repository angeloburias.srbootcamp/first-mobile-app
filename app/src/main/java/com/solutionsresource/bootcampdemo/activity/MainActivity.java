package com.solutionsresource.bootcampdemo.activity;

import static com.solutionsresource.bootcampdemo.util.Constants.REGISTER_REQUEST_CODE;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.solutionsresource.bootcampdemo.R;
import com.solutionsresource.bootcampdemo.model.Login;
import com.solutionsresource.bootcampdemo.model.Privilege;
import com.solutionsresource.bootcampdemo.model.User;
import com.solutionsresource.bootcampdemo.util.Constants;
import com.solutionsresource.bootcampdemo.util.OfflineDBHelper;
import com.solutionsresource.bootcampdemo.util.SharedPreference;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private OfflineDBHelper offlineDBHelper;
    private EditText editTextUsername, editTextPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean loggedIn = SharedPreference.getBooleanValue(this, "loggedIn");
        if (loggedIn) {
            startActivity(new Intent(MainActivity.this, PrivilegeActivity.class));
            finish();
        }

        offlineDBHelper = new OfflineDBHelper(this);
        editTextUsername = findViewById(R.id.editTextUsername);
        editTextPassword = findViewById(R.id.editTextPassword);

        Button btnLogin = findViewById(R.id.btnLogin);
        Button btnRegister = findViewById(R.id.btnRegister);

        btnLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REGISTER_REQUEST_CODE && data != null && data.getExtras() != null) {
            if (resultCode == Activity.RESULT_OK) {
                String name = data.getExtras().getString("name");
                User user = (User) offlineDBHelper.create(Constants.USER_TABLE, data.getExtras().getParcelable(User.TAG));
                if (user != null) {
                    Toast.makeText(this, "Successfully created " + name, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(this, "Failed to create user account", Toast.LENGTH_SHORT).show();
                }
            } else { // Activity.RESULT_CANCELED
                Toast.makeText(this, "Failed to create user account", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onClick(View vw) {
        switch (vw.getId()) {
            case R.id.btnLogin:
                User user = (User) offlineDBHelper.login(new Login(editTextUsername.getText().toString().trim(),
                        editTextPassword.getText().toString().trim()));
                if (user != null) {
                    SharedPreference.storeBooleanValue(MainActivity.this, "loggedIn", true);
//                    startActivity(new Intent(MainActivity.this, CalculatorActivity.class));

                    startActivity(new Intent(MainActivity.this, PrivilegeActivity.class));
                } else {
                    Toast.makeText(MainActivity.this, "Incorrect username and password", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.btnRegister:
                // not deprecated unless your using android x library
                startActivityForResult(new Intent(MainActivity.this, RegisterActivity.class), REGISTER_REQUEST_CODE);
                break;
            default:
                break;
        }
    }
}