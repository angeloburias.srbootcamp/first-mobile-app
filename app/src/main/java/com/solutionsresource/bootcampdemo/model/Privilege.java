package com.solutionsresource.bootcampdemo.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class Privilege {

    private String status, message, error;
    @SerializedName("datas")
    private List<Data> dataList;

    public Privilege(String status, String message, String error, List<Data> dataList) {
        this.status = status;
        this.message = message;
        this.error = error;
        this.dataList = dataList;
    }

    public List<Data> getDataList() {
        return dataList;
    }

    public void setDataList(List<Data> dataList) {
        this.dataList = dataList;
    }


    @lombok.Data
    public static class Data {

        private int id;
        private String name;

        public Data(int id, String name) {
            this.id = id;
            this.name = name;
        }

        public Data(String name) {
            this.name = name;
        }

    }
}
