package com.solutionsresource.bootcampdemo.util;

public class Constants {
    public static final String SHARED_PREFERENCE = "sharedPreference";

    public static final String OFFLINE_DATABASE_NAME = "demo_app.db";
    public static final int OFFLINE_DATABASE_VERSION = 2;
    public static final int REGISTER_REQUEST_CODE = 101;
    public static final String USER_TABLE = "user";
    public static final String ID = "id";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String NAME = "name";
    public static final String EMAIL_ADDRESS = "emailAddress";
    public static final String CONTACT_NUMBER = "contactNumber";
    public static final String GENDER = "gender";
}
